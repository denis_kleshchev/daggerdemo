package com.dinkl.daggeradvancedtest.main

import android.os.Bundle
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.dinkl.daggeradvancedtest.R
import com.dinkl.daggeradvancedtest.di.Injector
import com.dinkl.daggeradvancedtest.di.component.ActivityComponent
import com.dinkl.daggeradvancedtest.extensions.logDebug
import com.dinkl.daggeradvancedtest.extensions.mutableLazy
import com.dinkl.daggeradvancedtest.main.fragment.TestFragment

class MainActivity : MvpAppCompatActivity(), MainView {

    val tagg: String = javaClass.simpleName
    private var component: ActivityComponent? by mutableLazy { Injector.getActivityComponent(this) }

    @InjectPresenter
    lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction()
                .replace(R.id.container, TestFragment.newInstance())
                .commit()
    }

    override fun showTag() = Toast.makeText(this, tagg, Toast.LENGTH_SHORT).show()

    @ProvidePresenter
    fun providesPresenter(): MainPresenter {
        logDebug("try to provide presenter. component is $component")
        return component?.presenter() ?:
                throw IllegalStateException("Impossible to see this, it should crash in injector")
    }

    override fun onResume() {
        super.onResume()
        presenter.requestMessage()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (isFinishing) {
            Injector.releaseActivityComponent()
        }
    }
}
