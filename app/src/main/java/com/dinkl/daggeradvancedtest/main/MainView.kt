package com.dinkl.daggeradvancedtest.main

import com.arellomobile.mvp.MvpView

interface MainView: MvpView {
    fun showTag()
}