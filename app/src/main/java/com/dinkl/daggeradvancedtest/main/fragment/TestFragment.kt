package com.dinkl.daggeradvancedtest.main.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.dinkl.daggeradvancedtest.R
import com.dinkl.daggeradvancedtest.di.Injector
import com.dinkl.daggeradvancedtest.di.component.FragmentComponent
import com.dinkl.daggeradvancedtest.extensions.mutableLazy

class TestFragment : MvpAppCompatFragment(), FragmentView {
    val fragmentName = javaClass.simpleName

    private var component: FragmentComponent? by mutableLazy { Injector.getFragmentComponent(this) }

    @InjectPresenter
    lateinit var presenter: TestPresenter

    companion object {
        fun newInstance() = TestFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.test_fragment, container, false)

    override fun onDestroy() {
        super.onDestroy()
        if (isRemoving) {
            //close scope
            Injector.releaseFragmentComponent()
        }
    }

    @ProvidePresenter
    fun providePresenter() = component?.presenter() ?:
            throw IllegalStateException("Impossible to see this, it should crash in injector")

    override fun onResume() {
        super.onResume()
        presenter.requestMessage()
    }

    override fun showMessage() =
        Toast.makeText(context, "Fragment message $fragmentName", Toast.LENGTH_SHORT).show()
}