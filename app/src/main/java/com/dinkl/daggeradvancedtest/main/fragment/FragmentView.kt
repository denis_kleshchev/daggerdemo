package com.dinkl.daggeradvancedtest.main.fragment

import com.arellomobile.mvp.MvpView

interface FragmentView: MvpView {
    fun showMessage()
}