package com.dinkl.daggeradvancedtest.main

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.dinkl.daggeradvancedtest.di.scope.ActivityScope
import com.dinkl.daggeradvancedtest.extensions.logDebug
import com.dinkl.daggeradvancedtest.extensions.logInfo
import com.dinkl.daggeradvancedtest.helper.RxHelper
import io.reactivex.Single
import java.util.concurrent.TimeUnit

@ActivityScope
@InjectViewState
class MainPresenter(val rxHelper: RxHelper) : MvpPresenter<MainView>() {
    init {
        logInfo("I'm ${javaClass.simpleName}, my helper is ${System.identityHashCode(rxHelper)}")
    }

    fun requestMessage() {
        rxHelper.single.subscribe(Single.just(1)
                .delay(1, TimeUnit.SECONDS)
                .compose(rxHelper.single.applySchedulers()),
                {
                    logDebug("After delay!")
                    viewState.showTag()
                })

    }
}