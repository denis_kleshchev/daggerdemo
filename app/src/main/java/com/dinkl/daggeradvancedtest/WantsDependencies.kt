package com.dinkl.daggeradvancedtest

import android.content.Context
import com.dinkl.daggeradvancedtest.helper.RxHelper
import com.dinkl.daggeradvancedtest.di.scope.DomainScope
import javax.inject.Inject

@DomainScope
class WantsDependencies @Inject constructor(val context: Context,
                                            val helper: RxHelper) {
}