package com.dinkl.daggeradvancedtest.di.module

import android.content.Context
import android.os.AsyncTask
import com.dinkl.daggeradvancedtest.helper.RxHelper
import com.dinkl.daggeradvancedtest.App
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton

@Module
class AppModule(val app: App) {

    @Singleton
    @Provides
    fun providesContext(): Context = app

    @Provides
    fun providesRxHelper(): RxHelper =
            RxHelper(Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR), AndroidSchedulers.mainThread())

}