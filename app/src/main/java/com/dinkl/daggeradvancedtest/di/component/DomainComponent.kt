package com.dinkl.daggeradvancedtest.di.component

import com.dinkl.daggeradvancedtest.di.scope.DomainScope
import com.dinkl.daggeradvancedtest.helper.RxHelper
import dagger.Subcomponent

@DomainScope
@Subcomponent
interface DomainComponent {
    fun rxHelper(): RxHelper
}