package com.dinkl.daggeradvancedtest.di.component

import com.dinkl.daggeradvancedtest.di.module.MainActivityModule
import com.dinkl.daggeradvancedtest.di.scope.ActivityScope
import com.dinkl.daggeradvancedtest.helper.RxHelper
import com.dinkl.daggeradvancedtest.main.MainPresenter
import dagger.Component

@ActivityScope
@Component(dependencies = [DomainComponent::class], modules = [MainActivityModule::class])
interface ActivityComponent {
    fun rxHelper(): RxHelper
    fun presenter(): MainPresenter
}