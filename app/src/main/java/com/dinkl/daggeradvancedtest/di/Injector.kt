package com.dinkl.daggeradvancedtest.di

import com.dinkl.daggeradvancedtest.App
import com.dinkl.daggeradvancedtest.di.component.*
import com.dinkl.daggeradvancedtest.di.module.AppModule
import com.dinkl.daggeradvancedtest.di.module.FragmentModule
import com.dinkl.daggeradvancedtest.di.module.MainActivityModule
import com.dinkl.daggeradvancedtest.extensions.logDebug
import com.dinkl.daggeradvancedtest.main.MainActivity
import com.dinkl.daggeradvancedtest.main.fragment.TestFragment
import java.util.concurrent.atomic.AtomicBoolean

object Injector {
    private val initialized = AtomicBoolean(false)
    lateinit var appComponent: AppComponent
    lateinit var domainComponent: DomainComponent
    var activityComponent: ActivityComponent? = null
    var fragmentComponent: FragmentComponent? = null

    fun init(app: App) {
        logDebug("init, initialized: ${initialized.get()}")
        if (initialized.compareAndSet(false, true)) {
            appComponent = DaggerAppComponent
                    .builder()
                    .appModule(AppModule(app))
                    .build()
        }
    }

    private fun initActivityComponent(activity: MainActivity) {
        logDebug("initActivityComponent, was $activityComponent")
        activityComponent = DaggerActivityComponent
                .builder()
                .domainComponent(domainComponent)
                .mainActivityModule(MainActivityModule(activity))
                .build()
    }

    private fun initFragmentComponent(fragment: TestFragment) {
        fragmentComponent = DaggerFragmentComponent
                .builder()
                .activityComponent(activityComponent!!)
                .fragmentModule(FragmentModule(fragment))
                .build()
    }

    fun releaseActivityComponent() {
        fragmentComponent = null
        activityComponent = null
    }

    fun getActivityComponent(activity: MainActivity): ActivityComponent {
        if (activityComponent == null) {
            initActivityComponent(activity)
        }

        return activityComponent ?: throw IllegalStateException("activity component is null after initialization")
    }

    fun getFragmentComponent(fragment: TestFragment): FragmentComponent {
        if (fragmentComponent == null) {
            initFragmentComponent(fragment)
        }

        return fragmentComponent ?: throw IllegalStateException("fragment component is null after initialization")
    }

    fun releaseFragmentComponent() {
        fragmentComponent = null
    }
}