package com.dinkl.daggeradvancedtest.di.module

import com.dinkl.daggeradvancedtest.di.scope.FragmentScope
import com.dinkl.daggeradvancedtest.helper.RxHelper
import com.dinkl.daggeradvancedtest.main.fragment.TestFragment
import com.dinkl.daggeradvancedtest.main.fragment.TestPresenter
import dagger.Module
import dagger.Provides

@Module
class FragmentModule(val fragment: TestFragment) {

    @Provides
    fun providesTag(): String = fragment.fragmentName

    @FragmentScope
    @Provides
    fun providePresenter(rxHelper: RxHelper): TestPresenter = TestPresenter(rxHelper)
}