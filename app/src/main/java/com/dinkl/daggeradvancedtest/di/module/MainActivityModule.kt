package com.dinkl.daggeradvancedtest.di.module

import com.dinkl.daggeradvancedtest.helper.RxHelper
import com.dinkl.daggeradvancedtest.di.scope.ActivityScope
import com.dinkl.daggeradvancedtest.main.MainActivity
import com.dinkl.daggeradvancedtest.main.MainPresenter
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule(val activity: MainActivity) {

    @Provides
    fun providesTag() = activity.tagg

    @ActivityScope
    @Provides
    fun providesPresenter(rxHelper: RxHelper): MainPresenter = MainPresenter(rxHelper)
}