package com.dinkl.daggeradvancedtest.di.component

import com.dinkl.daggeradvancedtest.di.module.FragmentModule
import com.dinkl.daggeradvancedtest.di.scope.FragmentScope
import com.dinkl.daggeradvancedtest.main.fragment.TestPresenter
import dagger.Component
import dagger.Subcomponent

@FragmentScope
@Component(dependencies = [ActivityComponent::class], modules = [FragmentModule::class])
interface FragmentComponent {
    fun presenter(): TestPresenter
}