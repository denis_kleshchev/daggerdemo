package com.dinkl.daggeradvancedtest.di.component

import com.dinkl.daggeradvancedtest.di.module.AppModule
import com.dinkl.daggeradvancedtest.helper.RxHelper
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun rxHelper(): RxHelper
}