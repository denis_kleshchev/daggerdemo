package com.dinkl.daggeradvancedtest

import android.content.Context
import com.dinkl.daggeradvancedtest.helper.RxHelper
import com.dinkl.daggeradvancedtest.di.scope.ActivityScope
import javax.inject.Inject

@ActivityScope
class DependentActivity @Inject constructor(val context: Context,
                                            val helper: RxHelper) {
}