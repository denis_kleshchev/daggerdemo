package com.dinkl.daggeradvancedtest

import android.content.Context
import com.dinkl.daggeradvancedtest.helper.RxHelper
import com.dinkl.daggeradvancedtest.di.scope.FragmentScope
import javax.inject.Inject

@FragmentScope
class DependentFragment @Inject constructor(val context: Context,
                                            val helper: RxHelper) {
}