package com.dinkl.daggeradvancedtest

import android.app.Application
import com.dinkl.daggeradvancedtest.di.Injector
import com.dinkl.daggeradvancedtest.extensions.logDebug
import timber.log.Timber

class App: Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        logDebug("onCreate")
        Injector.init(this)
    }
}