package com.dinkl.daggeradvancedtest.helper

import io.reactivex.Flowable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subscribers.DisposableSubscriber

class FlowableHelper internal constructor(private val helper: CompositeHelper) {

    fun <T> subscribe(flowable: Flowable<T>): Disposable = helper.add(flowable.subscribe())

    fun <T> subscribe(flowable: Flowable<T>, next: (T) -> Unit): Disposable =
            helper.add(flowable.subscribe(next))

    fun <T> subscribe(flowable: Flowable<T>, next: (T) -> Unit, error: (Throwable) -> Unit): Disposable =
            helper.add(flowable.subscribe(next, error))

    fun <T> subscribe(flowable: Flowable<T>, next: (T) -> Unit, error: (Throwable) -> Unit, complete: () -> Unit): Disposable =
            helper.add(flowable.subscribe(next, error, complete))

    fun <T> subscribe(flowable: Flowable<T>, observer: DisposableSubscriber<T>): Disposable =
            helper.add(flowable.subscribeWith(observer))

    fun <T> bindPairSubject(flowable: Flowable<T>, subject: BehaviorSubject<T>): Disposable =
            subscribe(flowable, { subject.onNext(it) }, { subject.onError(it) })

    fun <T> subscribe(tag: String, flowable: Flowable<T>): Disposable =
            helper.add(tag, flowable.subscribe())

    fun <T> subscribe(tag: String, flowable: Flowable<T>, next: (T) -> Unit): Disposable =
            helper.add(tag, flowable.subscribe(next))

    fun <T> subscribe(tag: String, flowable: Flowable<T>, next: (T) -> Unit, error: (Throwable) -> Unit): Disposable =
            helper.add(tag, flowable.subscribe(next, error))

    fun <T> subscribe(tag: String, flowable: Flowable<T>, next: (T) -> Unit, error: (Throwable) -> Unit, complete: () -> Unit): Disposable =
            helper.add(tag, flowable.subscribe(next, error, complete))

    fun <T> subscribe(tag: String, flowable: Flowable<T>, observer: DisposableSubscriber<T>): Disposable =
            helper.add(tag, flowable.subscribeWith(observer))
}

fun CompositeHelper.flowableDelegate(): Lazy<FlowableHelper> = lazy {
    FlowableHelper(this)
}