package com.dinkl.daggeradvancedtest.helper

import io.reactivex.Maybe
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableMaybeObserver
import io.reactivex.subjects.BehaviorSubject

class MaybeHelper internal constructor(private val helper: CompositeHelper) {
    fun <T> subscribe(maybe: Maybe<T>): Disposable = helper.add(maybe.subscribe())

    fun <T> subscribe(maybe: Maybe<T>, next: (T) -> Unit): Disposable =
            helper.add(maybe.subscribe(next))

    fun <T> subscribe(maybe: Maybe<T>, next: (T) -> Unit, error: (Throwable) -> Unit): Disposable =
            helper.add(maybe.subscribe(next, error))

    fun <T> subscribe(maybe: Maybe<T>, next: (T) -> Unit, error: (Throwable) -> Unit, complete: () -> Unit): Disposable =
            helper.add(maybe.subscribe(next, error, complete))

    fun <T> subscribe(maybe: Maybe<T>, observer: DisposableMaybeObserver<T>): Disposable =
            helper.add(maybe.subscribeWith(observer))

    fun <T> bindPairSubject(maybe: Maybe<T>, subject: BehaviorSubject<T>): Disposable =
            subscribe(maybe, { subject.onNext(it) }, { subject.onError(it) })

    fun <T> subscribe(tag: String, maybe: Maybe<T>): Disposable = helper.add(tag, maybe.subscribe())

    fun <T> subscribe(tag: String, maybe: Maybe<T>, next: (T) -> Unit): Disposable =
            helper.add(tag, maybe.subscribe(next))

    fun <T> subscribe(tag: String, maybe: Maybe<T>, next: (T) -> Unit, error: (Throwable) -> Unit): Disposable =
            helper.add(tag, maybe.subscribe(next, error))

    fun <T> subscribe(tag: String, maybe: Maybe<T>, next: (T) -> Unit, error: (Throwable) -> Unit, complete: () -> Unit): Disposable =
            helper.add(tag, maybe.subscribe(next, error, complete))

    fun <T> subscribe(tag: String, maybe: Maybe<T>, observer: DisposableMaybeObserver<T>): Disposable =
            helper.add(tag, maybe.subscribeWith(observer))
}

fun CompositeHelper.maybeDelegate(): Lazy<MaybeHelper> = lazy {
    MaybeHelper(this)
}