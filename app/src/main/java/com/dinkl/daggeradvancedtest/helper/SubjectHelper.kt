package com.dinkl.daggeradvancedtest.helper

import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import java.util.concurrent.TimeUnit

class SubjectHelper internal constructor(private val helper: ObservableHelper) {

    fun <T> subscribe(subject: BehaviorSubject<Pair<T, Throwable>>, next: (T) -> Unit): Disposable =
            helper.subscribe(subject, { pair -> deserializeEmission(pair, next, { }) })

    fun <T> subscribe(subject: BehaviorSubject<Pair<T, Throwable>>, next: (T) -> Unit, error: (Throwable) -> Unit): Disposable =
            helper.subscribe(subject, { pair -> deserializeEmission(pair, next, error) })

    fun <T> subscribe(tag: String, subject: BehaviorSubject<Pair<T, Throwable>>, next: (T) -> Unit): Disposable =
            helper.subscribe(tag, subject, { pair -> deserializeEmission(pair, next, { }) })

    fun <T> subscribe(tag: String, subject: BehaviorSubject<Pair<T, Throwable>>, next: (T) -> Unit, error: (Throwable) -> Unit): Disposable =
            helper.subscribe(tag, subject, { pair -> deserializeEmission(pair, next, error) })


    fun <T> debounceWithBuffer(subject: Subject<T>, debounceTime: Long): Observable<List<T>> {
        val sharedSubject = subject.share()
        val debounce = sharedSubject.debounce(debounceTime, TimeUnit.MILLISECONDS)
        return sharedSubject.buffer(debounce)
    }

    fun <T> deserializeEmission(value: Pair<T, Throwable?>?, next: (T) -> Unit, error: (Throwable) -> Unit) {
        if (value != null) {
            when {
                value.first != null -> next.invoke(value.first)
                value.second != null -> error.invoke(value.second!!)
                else -> error.invoke(RuntimeException("pair with nulls"))
            }
        } else {
            error.invoke(RuntimeException("no pair"))
        }
    }
}

fun RxHelper.subjectDelegate(): Lazy<SubjectHelper> = lazy {
    SubjectHelper(observable)
}