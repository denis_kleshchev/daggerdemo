package com.dinkl.daggeradvancedtest.helper

import io.reactivex.Completable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.subjects.BehaviorSubject

class CompletableHelper internal constructor(private val helper: CompositeHelper) {

    fun <T> subscribe(completable: Completable): Disposable = helper.add(completable.subscribe())

    fun <T> subscribe(completable: Completable, next: () -> Unit): Disposable =
            helper.add(completable.subscribe(next))

    fun <T> subscribe(completable: Completable, next: () -> Unit, error: (Throwable) -> Unit): Disposable =
            helper.add(completable.subscribe(next, error))

    fun <T> subscribe(completable: Completable, observer: DisposableCompletableObserver): Disposable =
            helper.add(completable.subscribeWith(observer))

    fun <T> bindPairSubject(completable: Completable, subject: BehaviorSubject<Boolean>): Disposable =
            subscribe<Any>(completable, { subject.onNext(true) }, { subject.onError(it) })

    fun <T> subscribe(tag: String, completable: Completable): Disposable =
            helper.add(tag, completable.subscribe())

    fun <T> subscribe(tag: String, completable: Completable, next: () -> Unit): Disposable =
            helper.add(tag, completable.subscribe(next))

    fun <T> subscribe(tag: String, completable: Completable, next: () -> Unit, error: (Throwable) -> Unit): Disposable =
            helper.add(tag, completable.subscribe(next, error))

    fun <T> subscribe(tag: String, completable: Completable, observer: DisposableCompletableObserver): Disposable =
            helper.add(tag, completable.subscribeWith(observer))
}

fun CompositeHelper.completableDelegate(): Lazy<CompletableHelper> = lazy {
    CompletableHelper(this)
}