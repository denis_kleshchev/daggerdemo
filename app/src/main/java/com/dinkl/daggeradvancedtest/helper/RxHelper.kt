package com.dinkl.daggeradvancedtest.helper

import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.util.*
import javax.inject.Inject

class RxHelper @Inject constructor(val asyncScheduler: Scheduler,
                                   val mainScheduler: Scheduler) : CompositeHelper {

    val completeable by completableDelegate()
    val flowable by flowableDelegate()
    val maybe by maybeDelegate()
    val observable by observableDelegate()
    val single by singleDelegate()
    val subject by subjectDelegate()

    private val composite: CompositeDisposable = CompositeDisposable()
    private val map: MutableMap<String, Disposable> = HashMap()

    override fun add(disposable: Disposable): Disposable = disposable.apply {
        composite.add(this)
    }

    override fun add(tag: String, disposable: Disposable): Disposable = disposable.apply {
        composite.add(this)

        if (map.containsKey(tag)) {
            composite.remove(map.getValue(tag))
        }
        map.put(tag, disposable)
    }

    override fun remove(disposable: Disposable): Disposable = disposable.apply {
        composite.remove(disposable)
    }

    override fun remove(tag: String): Disposable? = if (map.containsKey(tag)) {
        map.getValue(tag).apply {
            remove(this)
            map.remove(tag)
        }
    } else {
        null
    }

    override fun clear() {
        composite.clear()
        map.clear()
    }

    override fun inProgress(disposable: Disposable?): Boolean = disposable?.isDisposed?.not() ?: false

    override fun inProgress(tag: String): Boolean = inProgress(map[tag])
}