package com.dinkl.daggeradvancedtest.helper

import io.reactivex.disposables.Disposable

interface CompositeHelper {
    fun add(disposable: Disposable): Disposable

    fun add(tag: String, disposable: Disposable): Disposable

    fun remove(disposable: Disposable): Disposable

    fun remove(tag: String): Disposable?

    fun clear()

    fun inProgress(disposable: Disposable?): Boolean

    fun inProgress(tag: String): Boolean
}