package com.dinkl.daggeradvancedtest.extensions

import org.slf4j.Logger
import org.slf4j.LoggerFactory

fun Any.lazyLogger(): Lazy<Logger> = lazy { LoggerFactory.getLogger(javaClass.simpleName) }

fun Any.logInfo(message: String) = lazyLogger().value.info(message)
fun Any.logDebug(message: String) = lazyLogger().value.debug(message)
fun Any.logWarn(message: String) = lazyLogger().value.warn(message)
fun Any.logError(message: String,
                 throwable: Throwable? = null) = lazyLogger().value.error(message, throwable)